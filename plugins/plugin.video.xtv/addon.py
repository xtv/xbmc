import sys
import xbmc
import xbmcgui
import xbmcaddon
import xbmcplugin
import CommonFunctions as common


# xbmc hooks
settings = xbmcaddon.Addon(id='plugin.video.myxtv')


if (__name__ == "__main__" ):
    import XTVService
    service = XTVService.Service()
    import XTVNavigation
    navigation = XTVNavigation.Navigation()
    
    try:
        navigation.showSettings(check = True)
        
        if (not sys.argv[2]):
            navigation.MainMenu()
        else:
            params = common.getParameters(sys.argv[2])
            navigation.subMenu(params)
    except Exception, ex:
        print ('[plugin.video.xtv] Uncaught exception in addon.py: %s' % str(ex) , xbmc.LOGDEBUG)   
