#
#      Copyright (C) 2013 Team XTV
#      http://www.xtv.gr
#
#  This Program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.
#
#  This Program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this Program; see the file LICENSE.txt.  If not, write to
#  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#  http://www.gnu.org/copyleft/gpl.html
#

import os
import xbmcaddon
import notification
import xbmc
import time
import source
import home
import xtvSettings 
import shutil
import distutils.core

class Service(object):
    def __init__(self):
        self.database = source.Database()
        self.database.initialize(self.onInit)

    def onInit(self, success):
        if success:
            self.database.updateChannelAndProgramListCaches(self.onCachesUpdated)
        else:
            self.database.close()

    def update(self):
        self.database.updateChannelAndProgramListCaches(self.onCachesUpdated)
        
    def onCachesUpdated(self):
        ADDON = xbmcaddon.Addon(id = 'script.xtv.stb')

        if ADDON.getSetting('notifications.enabled') == 'true':
            n = notification.Notification(self.database, ADDON.getAddonInfo('path'))
            n.scheduleNotifications()

        self.database.close(None)
        ADDON = None
    
    def close(self):
        if(self.database):
            self.database.close()

def OverwriteRSS():
    filename = xbmc.translatePath(os.path.join('special://userdata','RssFeeds.xml'))
    f = open(filename, 'r+')
    f.seek(0)
    f.writelines('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n')
    f.writelines('<rssfeeds>\n')
    f.writelines('  <set id="1">\n')
    f.writelines('    <feed updateinterval="20">http://www.greeknamedays.gr/tools/eortologiorssfeed/index.php?langid=gr</feed>\n')
    f.writelines('    <feed updateinterval="20">http://www.trtgreek.com/rss/gundem.rss?dil=gr</feed>\n')
    f.writelines('    <feed updateinterval="20">http://ws.kathimerini.gr/xml_files/news.xml</feed>\n')
    f.writelines('    <feed updateinterval="20">http://feeds.feedburner.com/skai/yinm</feed>\n')
    f.writelines('    <feed updateinterval="20">http://feeds.feedburner.com/skai/TfmK</feed>\n')
    f.writelines('    <feed updateinterval="20">http://www.enet.gr/rss?i=news.el.article</feed>\n')
    f.writelines('  </set>\n')
    f.writelines('</rssfeeds>\n')
    f.truncate()
    f.close()
#    xbmc.executebuiltin("RefreshRSS()")

def OverwriteKeyMap():
    overwrite = True
    filename = xbmc.translatePath(os.path.join('special://userdata','keymaps/keyboard.xml'))
    print("<!****[script.xtv.stb] - %s" % filename)
    
    if(os.path.exists(filename)):
        overwrite = False
    
    if(overwrite):
        xbmc.executebuiltin("Notification(Information,%s,5000)" % "Updating XBMC")
        f = open(filename, 'w+')
        f.seek(0)
        f.writelines('<?xml version="1.0" encoding="UTF-8"?>\n')
        f.writelines('<keymap>\n')
        f.writelines('  <FullscreenVideo>\n')
        f.writelines('      <keyboard>\n')
        f.writelines('          <left>Stop</left>\n')
        f.writelines('          <right>Stop</right>\n')
        f.writelines('          <up>Stop</up>\n')
        f.writelines('          <down>Stop</down>\n')
        f.writelines('      </keyboard>\n')
        f.writelines('  </FullscreenVideo>\n')
        f.writelines('</keymap>\n')
        f.truncate()
        f.close()
        xbmc.executebuiltin("ReloadSkin()")
    return overwrite

def OverwriteConfluence():
    fromDirectory = xbmc.translatePath(os.path.join('special://xbmc','addons/skin.confluence'))
    toDirectory = xbmc.translatePath(os.path.join('special://home','addons/skin.confluence'))
    fromHomeFile = xbmc.translatePath(os.path.join('special://home','addons/script.xtv.stb/resources/skins/Default/Home.xml'))
    toHomeFile = xbmc.translatePath(os.path.join('special://home','addons/skin.confluence/720p/Home.xml'))
    
    if not os.path.exists(toDirectory):
        xbmc.executebuiltin("Notification(Information,%s,5000)" % "Updating XBMC")
        distutils.dir_util.copy_tree(fromDirectory, toDirectory)
        
        # alter home screen
        shutil.copyfile(fromHomeFile,toHomeFile)
        xbmc.executebuiltin("ReloadSkin()")
        xbmc.executebuiltin("RestartApp()")
    
try:
    #       secs * minutes * hours
    sleep = 60 * 60 * 1
    OverwriteRSS()
    OverwriteKeyMap()
#    OverwriteConfluence()
    
    settings = xtvSettings.xtvSettings()
    print "Updating Home Menu"
    updater = home.homeUpdater(settings)
    updater.OverwriteHome()
#    xbmc.executebuiltin("RunAddon(plugin.video.xtv)")
    
#  We will take this out, too often it fails due to a lock on date parsing because of other scripts trying to process on startup
#    ADDON = xbmcaddon.Addon(id = 'script.tvguide')
#    if settings.CacheOnStart():
#        Service()
    
    if (settings.userName() == '' or settings.userPassword() == ''):    
        xbmc.executebuiltin("RunPlugin(plugin://plugin.video.myxtv?action=settings)")
    else:
        if(settings.AutoStart()):
    #        xbmc.executebuiltin("ActivateWindow(settings)")
        #    xbmc.executebuiltin("RunAddon(plugin.video.xtv)")
            xbmc.executebuiltin("RunScript(script.xtv.stb)")
    
    time.sleep(360)
    print ('************** [script.xtv.stb] Updating Guide Cache ****************')
    dbCache = Service()
    while (not xbmc.abortRequested):
        time.sleep(sleep)
        print ('************** [script.xtv.stb] Updating Guide Cache ****************')
        dbCache.update()
        
    dbCache.close()
except source.SourceNotConfiguredException:
    pass  # ignore

except Exception, ex:
    print ('[script.xtv.stb] Uncaught exception in service.py: %s' % str(ex) , xbmc.LOGDEBUG)


