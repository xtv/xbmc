import os
import sys
import urllib
import xbmc
import xbmcgui
import time
from uuid import uuid4

class Navigation():
    def __init__(self):
        self.xbmc = sys.modules["__main__"].xbmc
        self.xbmcgui = sys.modules["__main__"].xbmcgui
        self.xbmcplugin = sys.modules["__main__"].xbmcplugin

        self.settings = sys.modules["__main__"].settings
        self.service = sys.modules["__main__"].service

        self.settingsItem = {'Title':'Settings'  , 'Logo':self.service.getPluginImage('settings'), 'action':"settings" }

    def showSettings(self, check = False):
        show = False
        
        if(check):
            if (not self.settings.getSetting("firstrun")):
                show = True
                self.settings.setSetting("firstrun", "1")
        
            if (self.service.userName() == ""):
                show = True
                
            if (self.service.userPassword() == ""):
                show = True
        
            mac_address = xbmc.getInfoLabel('Network.MacAddress')
            if not ":" in mac_address:
                time.sleep(2)
                mac_address = xbmc.getInfoLabel("Network.MacAddress")
                
            if (not self.settings.getSetting("appId")):
                show = True
                self.settings.setSetting("appId", mac_address)

            elif self.settings.getSetting("appId") != mac_address:
                    self.settings.setSetting("appId", mac_address)
                    
#        old_user_name = self.service.userName()
#        old_user_password = self.service.userPassword()
        else:
            show = True
            
        if(show):
            self.settings.openSettings()
    
            user_name = self.service.userName()
            user_password = self.service.userPassword()
    
            result = ""
            status = 500
    
            if not user_name:
                return (result, 200)
    
            self.xbmc.executebuiltin("Container.Refresh")
            return (result, status)

    def MainMenu(self):
#        self.CategoryMenu()
        self.addListItem(self.settingsItem)
        self.xbmcplugin.endOfDirectory(handle=int(sys.argv[1]), succeeded=True, cacheToDisc=False)

    def subMenu(self, params={}):
        get = params.get

        if (not get("action")):
            self.addListItem(self.settingsItem)    
        else:
            if (get("action") == 'settings'):
                self.showSettings()
            else:
                print plugin + " ARGV Nothing done.. verify params " + repr(params)

    def addListItem(self, params={}):
        item = params.get
        folder = True

        icon = item("Logo", self.service.getPluginImage('explore'))
        thumbnail = item("Logo", self.service.getPluginImage('explore'))

        listitem = self.xbmcgui.ListItem(item("Title"), iconImage=icon, thumbnailImage=thumbnail)

        url = '%s?path=%s&' % (sys.argv[0], item("path"))
        url += 'action=' + item("action") + '&'
        url += 'Title=' + item("Title") + '&'
        if (item("action") == "category"):
            url += 'id=' + item("CategoryID") + '&'
        if (item("action") == "channel"):
            url += 'id=' + item("ChannelID") + '&'
        if (item("Logo")):
            url += 'Logo=' + item("Logo") + '&'

        self.xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]), url=url, listitem=listitem, isFolder=folder)
